#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jun  1 22:19:16 2020

"""

import matplotlib.pyplot as plt
import numpy as np

frequency = [6,10,30,60,100,300]
depth = [8.1,3.9,0.92,0.49,0.35,0.23]

plt.plot(frequency, depth,'o-')
plt.fill_between([24,100],[8,8],color='r',alpha=0.3)
plt.xlabel('Հաճախություն (ԳՀց)', fontsize=16)
plt.ylabel('Թափանցելիություն (մմ) ', fontsize=16)
plt.xticks(fontsize=14)
plt.yticks(fontsize=14)
plt.title('Մաշկի ի՞նչ խորության են հասնում ռադիոալիքները',fontsize=16)
plt.savefig('frequencyVSdepth.svg')


x=np.linspace(0,2,20)
x=np.concatenate((x,x[::-1]))

patterns = [ "//" ]
for ind, i in enumerate(x[:-1]):
    fig,ax = plt.subplots()
    ax.scatter([x[0],i],[1,1],s=[100,1000],color='purple')
    ax.bar(0, 2, width=10, color='white', edgecolor='black', alpha=.4,hatch=patterns[0])
    ax.set_xlim(-0.1,2)
    ax.set_xticks([])
    ax.set_yticks([])
    fig.savefig(f'dot{ind}.png')
